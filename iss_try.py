#from sys import last_traceback
from urllib.request import urlopen
import json
import datetime
import time

from numpy import double


def rightTime(sunrise, sunset):
    # jaky je cas pro porovnani s vychodem a zapadem
    now = datetime.datetime.now()

    
    sunrise_time = datetime.datetime.strptime(now.strftime("%m/%d/%Y")+" "+sunrise, '%m/%d/%Y %I:%M:%S %p')
    sunset_time = datetime.datetime.strptime(now.strftime("%m/%d/%Y")+" "+sunset, '%m/%d/%Y %I:%M:%S %p')

    #ziskany cas je v UTC, CR je v UTC+1 -> potřeba přidat jednu hodinu
    sunrise_sec = sunrise_time.hour*3600+sunrise_time.minute*60+sunrise_time.second     +3600
    sunset_sec = sunset_time.hour*3600+sunset_time.minute*60+sunset_time.second         +3600
    #v now je ulozen cas ktery zohlednuje UTC+1
    now_sec = now.hour*3600+now.minute*60+now.second

    duration_sunrise = now_sec - sunrise_sec
    # spravny cas zacina, pokud zbyvaji do vychodu dve hodiny dokud neni vychod
    hours_sunrise = duration_sunrise/3600.0
    if hours_sunrise > -2 and hours_sunrise < 0:
        return True

    duration_sunset = now_sec - sunset_sec
    # spravny cas po zapadu je od zapadu do dvou hodin po nem
    hours_sunset = duration_sunset/3600.0

    if hours_sunset > 0 and hours_sunset < 2:
        return True

    return False


def rightPlace(lat,lon):
    #nastaveni pozorovatelneho uhlu (tohle je v podstate polovina)
    #nevim jak velky uhel je pozorovatelny - tato hodnota by se musela upravit
    zorny_uhel = 15
    # cz - - > lat = 49.8203, lon = 15.4784
    czLat = 49.8203
    czLon = 15.4784
    if abs(czLat-double(lat)) < zorny_uhel and abs(czLon-double(lon)) < zorny_uhel:
        return True
    return False


def get_iss():
    req = "http://api.open-notify.org/iss-now.json"
    response = urlopen(req)
    obj = json.loads(response.read())
    if obj['message'] != "success":
        print("Couldnt connect to ISS station")
        exit()

    return obj


def get_srss():
    #request nastaven na dnesek v CR
    req = "https://api.sunrise-sunset.org/json?lat=49.8203&lng=15.4784&date=today"
    response = urlopen(req)
    obj = json.loads(response.read())
    if obj['status'] != "OK":
        print("Couldnt connect to sunrise-sunset api")
        exit()
    return obj





###############################################---MAIN---###########################################
iss = get_iss()
srss = get_srss()
lat = iss['iss_position']['latitude']
lon = iss['iss_position']['longitude']
iss_time = iss['timestamp']
sunrise = srss['results']['sunrise']
sunset = srss['results']['sunset']


czLat = 49.8203
czLon = 15.4784
rt = rightTime(sunrise, sunset)
rp = rightPlace(lat,lon)


if rt and rp:
    print("Now is the perfect time for you to watch ISS station cyrceling around the orbit.")

if not rt:
    print("Now is not the right time to watch ISS station. Wait for after the sunset ("+sunset+"), or before sunrise("+sunrise+").")

if not rp:
    print("ISS is not above you right now. Its latitude is ", lat, " and longitude is ",
          lon, "\nCzech Republic\'s latitude is about ", czLat, " and longitude is ", czLon, ".")
