# 3. Úkol – REST klient
## REST klient „ISS now“
Implementujte REST klienta, který zjistí aktuální pozici mezinárodní vesmírné stanice (ISS) a
zobrazí informaci o tom, zda se nachází na aktuálně osvětlené či neosvětlené straně Země.
Stejně tak vypíše i informaci o tom, zda jsou na daném místě ideální podmínky pro její
pozorování z povrchu Země, tj. 1-2 hodiny před východem nebo po západu Slunce.
Pro realizaci použijte níže uvedená veřejná REST API, pro jednoduchost pracujte pouze
s UTC časovou zónou.

## Spuštění
Spouštění probíhá přes command line. Pro spuštění programu lze použít:
```sh
python iss_try.py
```
případně:
```sh
python3 iss_try.py
```

Program by měl uživateli vypsat výsledek výpočtu a ukončit proces. Výsledkem je myšleno jestli je momentálně ISS stanice pozorovatelná z území České Republiky, tedy jestli je na správném místě a je pro pozorování správný čas. Výsledek je vypsán v anglickém jazyce jednou nebo více větami, které informují uživatele o tom, jestli je správná chvíle, nebo proč není.

## Implementace
Při práci s api na východ a západ slunce jsem měl drobné problémy s parsováním získaného výsledku, ale nástroje pythonu mi práci značně usnadnily. Druhým zádrhelem při implementaci byla absence informace o tom, v jakém zorném úhlu je stanice nad zemí pozorovatelná, případně jaká má být odchylka zeměpisné výšky a šířky mezi stanicí a uživatelem (v mém případě Českou Republikou) pro to, aby mohla být stanice označena za pozorovatelnou.

## Závěr
Krom původního zmatení, které bylo způsobeno mou nepozorností a vyřešeno na cvičení, proběhla implementace řešení bez větších problémů. Jelikož cílem práce bylo vyzkoušení si získání informací a jejich následné zpracování, věřím, že jsem zadání splnil.
